#基于哪个镜像
FROM harbor.yuncloud.cn/library/edas:3.0-fonts
#将本地文件夹挂在到当前容器
VOLUME /tmp
#复制文件到容器
ADD ruoyi-admin/target/ruoyi-admin.jar /home/admin/app/app.jar
RUN echo "Asia/Shanghai" > /etc/timezone
#指定运行的用户
USER root